<?php

namespace App\Http\Controllers;

use App\Contact;

class ContactsController extends Controller
{
    public function create()
    {
        Contact::create([
            'name' => request('name'),
            'email' => request('email'),
        ]);

        return response()->json([], 201);
    }
}
