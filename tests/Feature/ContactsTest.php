<?php

namespace Tests\Feature;

use App\Contact;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ContactsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function creating_a_contact()
    {
        $this->withoutExceptionHandling()
            ->postJson("/api/contacts", [
                'name' => "John Doe",
                'email' => "john@example.com",
            ])
            ->assertStatus(201);

        $this->assertCount(1, Contact::all());

        $contact = Contact::first();
        $this->assertEquals("John Doe", $contact->name);
        $this->assertEquals("john@example.com", $contact->email);
    }
}
